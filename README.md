# 一步步从点灯程序做到两栖四轴飞行船

#### 介绍
一步步从点灯到两栖四轴飞行器制作

前期基于ATMEGA4809单片机，在Microchip Studio工具中使用C语言开发，后面根据需要需作选择。
本项目开始由一个个简单的实验开始，最终完成一个既能在水里游又能在地上跑，还能飞行的四轴飞行器。 

LightUp：往PE02引脚交替输出高电平和低电频，使ATMEGA4809上的led灯闪烁。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
