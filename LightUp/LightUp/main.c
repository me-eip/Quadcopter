/*
 * LightUp.c
 * 点灯实验
 * Created: 2022/3/19 18:38:18
 * Author : DELL
 */ 

#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>

int main(void)
{
	PORTE_DIRSET |= (1 << 2);   // PC03引脚设置为输出引脚
    while (1) 
    {
		PORTE_OUT |= (1 << 2); // height PC03引脚输出高电频
		_delay_ms(100);
		PORTE_OUT &= ~(1 << 2); //low  PC03引脚输出低电频
		_delay_ms(100);
    }
	return 1;
}

